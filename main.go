/*
This tool cuts excess Ns off of either the start or end of a given sequence;
returning the longest of the two possibilities.
*/
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"unicode"
)

func main() {
	cutoff := flag.Float64(
		"cutoff",
		0.5,
		"The N-percentage to exceed before cutting.",
	)
	dir := flag.String(
		"directory",
		".",
		"Directory to search for files in.",
	)
	ext := flag.String(
		"extension",
		".seq",
		"Extension to look for.",
	)
	outfasta := flag.String(
		"outfasta",
		"",
		"The FASTA file to output to.",
	)
	shortest := flag.Bool(
		"shortest",
		false,
		"Whether to prefer shortest cut sequence.",
	)

	flag.Parse()

	flag.Usage()

	out := new(os.File)
	defer out.Close()
	if *outfasta != "" {
		o, err := os.Create(*outfasta)
		if err != nil {
			panic(fmt.Errorf("%v", err))
		} else {
			out = o
		}
	} else {
		out = os.Stdout
	}

	files := checkExt(*dir, *ext)
	for _, path := range files {
		precontent, err := ioutil.ReadFile(path)
		if err != nil {
			panic(fmt.Errorf("Corrupt file at %s", path))
		}
		content := string(precontent)

		content = strings.Map(func(r rune) rune {
			if unicode.IsSpace(r) {
				return -1
			}
			return r
		}, content)

		cut := CutNs(content, *cutoff, *shortest)
		filename := filepath.Base(path)
		if len(content) == len(cut) {
			fmt.Printf("%s never exceeded the cutoff, no cuts were made.\n", filename)
		}
		out.WriteString(fmt.Sprintf("> %s\n", filename))
		out.WriteString(cut + string('\n'))
	}
	return
}

// checkExt takes a root path and a desired extension to produce a file list of all files with that
// extension below that root path (i.e. the directory is walked down to all children)
func checkExt(pathS, ext string) []string {
	var files []string
	filepath.Walk(pathS, func(path string, f os.FileInfo, _ error) error {
		if !f.IsDir() {
			if filepath.Ext(path) == ext {
				absPath, err := filepath.Abs(path)
				if err != nil {
					panic(err)
				} else {
					files = append(files, absPath)
				}
			}
		}
		return nil
	})
	return files
}

// CutNs returns either the longest or shortest sequence after
// cutting Ns/ns out of the sequence when the percentage encountered
// exceeds the specified cutoff.
func CutNs(seq string, cutoff float64, shortest bool) string {
	forwseq := seq
	backseq := seq
starttoend:
	ncount := 0.0
	for num := 0; num < len(forwseq); num++ {
		if forwseq[num] == 'N' || forwseq[num] == 'n' {
			ncount++
			if float64(ncount)/(float64(num)+1.0) > cutoff {
				forwseq = forwseq[num+1:]
				goto starttoend
			}
		}
	}
endtostart:
	ncount = 0.0
	for num := len(backseq) - 1; 0 <= num; num-- {
		if backseq[num] == 'N' || backseq[num] == 'n' {
			ncount++
			if float64(ncount)/(float64(num)+1.0) > cutoff {
				backseq = backseq[:num]
				goto endtostart
			}
		}
	}

	if len(forwseq) <= len(backseq) == shortest {
		return forwseq
	} else {
		return backseq
	}
}
