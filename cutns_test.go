package main

import (
	"math/rand"
	"testing"
	"time"

	"github.com/leanovate/gopter"
	"github.com/leanovate/gopter/gen"
	"github.com/leanovate/gopter/prop"
)

func TestCutNsTable(t *testing.T) {
	table := []struct {
		in       string
		cutoff   float64
		shortest bool
		expected string
	}{
		{"ATGCN", 0.15, false, "ATGC"},
		{"NATGC", 0.15, false, "ATGC"},
		{"NATGC", 0.15, true, ""},
		{"ATGCN", 0.15, true, ""},
		{"NATGC", 0.20, false, "ATGC"},
		{"ATGCN", 0.20, false, "ATGCN"},
	}

	for _, test := range table {
		cutstring := CutNs(test.in, test.cutoff, test.shortest)
		if cutstring != test.expected {
			t.Errorf(
				"CutNs(%s, %f, %v) == %s, not expected %s",
				test.in,
				test.cutoff,
				test.shortest,
				cutstring,
				test.expected,
			)
		}
	}
}

func TestCutNsCutsMonoNs(t *testing.T) {
	parameters := gopter.DefaultTestParameters()
	parameters.MinSuccessfulTests = 100
	properties := gopter.NewProperties(parameters)

	properties.Property("Sequences of only Ns should cut to empty.", prop.ForAll(
		func(args []interface{}) bool {
			seq := randStringViaRunes(args[0].(uint), []rune("Nn"))
			return CutNs(seq, args[1].(float64), args[2].(bool)) == ""
		},
		gopter.CombineGens(
			gen.UIntRange(1, 200),      // Length of generated sequence
			gen.Float64Range(0.0, 1.0), // N-percent cutoff
			gen.Bool(),                 // Prefer shortest?
		),
	))
	properties.Property("CutNs should not cut sequences with no Ns", prop.ForAll(
		func(args []interface{}) bool {
			seq := randStringViaRunes(args[0].(uint), []rune("ATGCatgc"))
			return CutNs(seq, args[1].(float64), args[2].(bool)) != ""
		},
		gopter.CombineGens(
			gen.UIntRange(1, 200),      // Length of generated sequence
			gen.Float64Range(0.0, 1.0), // N-percent cutoff
			gen.Bool(),                 // Prefer shortest?
		),
	))
	properties.TestingRun(t)
}

func randStringViaRunes(n uint, valid []rune) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = valid[rand.Intn(len(valid))]
	}
	return string(b)
}
